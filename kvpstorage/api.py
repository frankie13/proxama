from tastypie.resources import ModelResource
from tastypie.constants import ALL
from models import KeyValue
from models import Token

class RegisterResource(ModelResource):
	'''Creates a token.
	Used to register application for the first time. 
	Returns a unique identifier used to identify
	application KeyValue objects. 
	'''
	class Meta:
		queryset = Token.objects.all()
		resource_name = "registerapplication"
		
	def obj_get_list(self, bundle, **kwargs):
		if bundle.request.GET['register'] == "true":
    			t = Token()
    			t.save()
    			#send token back to client
    			objects = Token.objects.filter(token=t.token);
		return objects
	
class StoreResource(ModelResource):
	'''Used to store a new KeyValue object in the database.
	Client specifys a token, key and value.
	Objects are then returned to client.
	'''
	class Meta:
		queryset = KeyValue.objects.all()
		resource_name="storedata"
		
	def obj_get_list(self, bundle, **kwargs):
    		token = bundle.request.GET['token']
    		key = bundle.request.GET['key']
    		value = bundle.request.GET['value']
    		
    		#create a KeyValue with incoming fields providing token matches. 
    		keyValue = KeyValue.objects.create_keyValue(token, key, value)
    		keyValue.save()
    		objects = KeyValue.objects.filter(id=keyValue.id);
		return objects	

class RetrieveResource(ModelResource):
	'''Used to access KeyValue objects. 
	Client specifys a token and an optional key.
	Relevent objects are then returned to the client.
	If no key is specified then all objects that match 
	the token are returned to the client.'''
	class Meta:
		queryset = KeyValue.objects.all()
		resource_name = "retrievedata"

	def obj_get_list(self, bundle, **kwargs):
			tokenInst = Token.objects.get(token=bundle.request.GET['token'])
			if 'key' in bundle.request.GET:
				key =  bundle.request.GET['key']
				objects = KeyValue.objects.filter(token=tokenInst, key=key);
			else:
				objects = KeyValue.objects.filter(token=tokenInst)
				
			return objects


		