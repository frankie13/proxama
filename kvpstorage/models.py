from django.db import models
import uuid

class KeyValueManager(models.Manager):
    '''Used to create KeyValue objects'''
    def create_keyValue(self, token, key, value):
                tokenInst = Token.objects.get(token=token)
                keyValue = self.create(token=tokenInst, key=key, value=value)
                return keyValue

class Token(models.Model):
        '''Used to identify application'''
        token = models.CharField(max_length=100, blank=True, unique=True, default=uuid.uuid4)

class KeyValue(models.Model):
    '''Contains a key value pair'''
    token = models.ForeignKey(Token)
    key = models.CharField(max_length=20)
    value = models.CharField(max_length=100)
    objects = KeyValueManager()
    def save(self, *args, **kwargs):
        if len(self.key) < 1:
            raise Exception('Key must be between 1 and 20 characters.')
        else:
            super(KeyValue, self).save(*args, **kwargs)
        #not checking min length on value as it could be zero