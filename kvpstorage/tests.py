from tastypie.test import ResourceTestCase
from models import KeyValue
from models import Token

class ResourceTest(ResourceTestCase):
            
    key = "key123"
    value = "value123"
    
    def setUp(self):
        super(ResourceTest, self).setUp()
    
    def test_registerapplication(self):
        '''Tests you can register and get a valid response'''
        self.assertEqual(0, len(Token.objects.all()))
        response = self.api_client.get('/api/registerapplication/', format='json', data={'register':'true'})
        self.assertValidJSONResponse(response)
        self.assertEqual(1, len(Token.objects.all()))
        self.assertEqual(len(self.deserialize(response)['objects']),1)

        #TODO: deserialise http response and compare with stored token
        
    def test_storedata(self):
        '''Tests you can store data with a GET style url. Depends on register'''
        self.test_registerapplication()
        tokenInst = Token.objects.get(id=1)
        storeResponse = self.api_client.get('/api/storedata/', format='json', data={'token':tokenInst.token, 'key' : self.key, 'value' : self.value})
        self.assertValidJSONResponse(storeResponse)
        self.assertEqual(1, len(KeyValue.objects.all()))
        storedKeyValue = KeyValue.objects.get(id=1)
        self.assertEqual(self.key, storedKeyValue.key)
        self.assertEqual(self.value, storedKeyValue.value)
        
    def test_retrievedata(self):
        '''Tests you can retrieve data with a GET style url for a defined key and no key. Depends on register and store'''
        key2 = 'key456'
        key3 = 'key789'
        value2 = 'value456'
        value3 = 'value789'
        
        self.test_storedata()
        tokenInst = Token.objects.get(id=1)
        #another record with the same key
        storeResponse = self.api_client.get('/api/storedata/', format='json', data={'token':tokenInst.token, 'key' : self.key, 'value' : value2})
        #create more records with different keys
        self.api_client.get('/api/storedata/', format='json', data={'token':tokenInst.token, 'key' : key2, 'value' : value2})
        self.api_client.get('/api/storedata/', format='json', data={'token':tokenInst.token, 'key' : key3, 'value' : value3})
        #call with key
        retrieveResponse = self.api_client.get('/api/retrievedata/', format='json', data={'token':tokenInst.token, 'key' : self.key})
        self.assertValidJSONResponse(retrieveResponse)
        self.assertEqual(len(self.deserialize(retrieveResponse)['objects']),2)
        #TODO: get hold of actual objects from the httpresponse to compare
        
        #call with no key
        retrieveResponseNoKey = self.api_client.get('/api/retrievedata/', format='json', data={'token':tokenInst.token})
        self.assertValidJSONResponse(retrieveResponseNoKey)
        self.assertEqual(len(self.deserialize(retrieveResponseNoKey)['objects']),4)
        

        
        