from django.conf.urls import patterns, include, url
from kvpstorage.api import StoreResource
from kvpstorage.api import RegisterResource
from kvpstorage.api import RetrieveResource

store_resource = StoreResource()
register_resource = RegisterResource()
retrieve_resource = RetrieveResource()

urlpatterns = patterns('',
	url(r'^api/', include(store_resource.urls)),
	url(r'^api/', include(register_resource.urls)),
	url(r'^api/', include(retrieve_resource.urls)),
)
